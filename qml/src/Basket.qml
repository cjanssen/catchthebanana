import QtQuick 2.0

Item {
    // 52 x 47
    width: 52
    height: 480
    id: basket

    property int basketTop: basketpic.y

    Image {
        id: basketpic
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 25
        source: "../../images/basket.png"

        Text {
            anchors.centerIn: parent
            color: "white"
            font.pixelSize: 32
            font.family: "Comic Sans"
            text: score
        }
    }

    Repeater {
        model: score
        delegate: BananaMarker {
            anchors.horizontalCenter: basket.horizontalCenter
            y: 385 - index*24
        }
    }

}
