import QtQuick 2.0

Image {
    id: banana
    source: "../../images/banana.png"

    property int ndx: 0

    property int monkeyY: monkey.y
    onMonkeyYChanged: monkeyTouched();

    Connections {
        target: rootWindow
        onResetItems: createBanana();
    }

    property int xpos: 0
    property int ypos: 0
    x: xpos - monkey.x * frontScale
    y: ypos
    property int initialRotation: 0

    state: "invisible"

    states: [
        State {
            name: "invisible"
            PropertyChanges {
                target: banana
                visible: false
            }
        },
        State {
            name: "appearing"
        },
        State {
            name: "normal"
        },
        State {
            name: "falling"
        },
        State {
            name: "catched"
        }
    ]

    Timer {
        id: resettingTimer;
        running: false
        interval: 5000
        repeat: false
        onTriggered: respawnBanana();
    }

    Timer {
        id: lifeTimer;
        running: false
        interval: 5000
        repeat: false
        onTriggered: bananaFall();
    }

    SequentialAnimation {
        id: bananaCatchAnimation
        property int animDuration: 1000
        ParallelAnimation {
            RotationAnimation {
                target: banana
                property: "rotation"
                to: 1200
                duration: bananaCatchAnimation.animDuration
            }
            PropertyAnimation {
                target: banana
                property: "xpos"
                to: basket.x + monkey.x * frontScale
                duration: bananaCatchAnimation.animDuration
            }
            PropertyAnimation {
                target: banana
                property: "ypos"
                to: basket.basketTop
                duration: bananaCatchAnimation.animDuration
            }
        }
        ScriptAction {
            script: {
                banana.state = "invisible"
                score = score + 1;
            }
        }
    }

    SequentialAnimation {
        id: bananaAppearAnimation
        PropertyAnimation {
            target: banana
            property: "scale"
            easing.type: Easing.OutElastic
            from: 0
            to: 1
            duration: 2000
        }
        ScriptAction {
            script: {
                banana.state = "normal"
                lifeTimer.start();
            }
        }
    }


    SequentialAnimation {
        id: bananaFallAnimation
        property int animationDuration: 1500

        SequentialAnimation {
            loops: 8
            RotationAnimation {
                target: banana
                property: "rotation"
                direction: RotationAnimation.Counterclockwise
                to: initialRotation - 15
                duration: 100
            }
            RotationAnimation {
                target: banana
                property: "rotation"
                direction: RotationAnimation.Clockwise
                to: initialRotation
                duration: 100
            }
        }
        ScriptAction {
            script: {
                banana.state = "falling"
            }
        }
        ParallelAnimation {
            RotationAnimation {
                target: banana
                property: "rotation"
                direction: RotationAnimation.Counterclockwise
                to: rotation - 360
                duration: bananaFallAnimation.animationDuration
            }
            PropertyAnimation {
                target: banana
                property: "ypos"
                easing.type: Easing.InQuad
                to: rootWindow.height + ypos
                duration: bananaFallAnimation.animationDuration
            }
        }
        ScriptAction {
            script: {
                banana.state = "invisible"
                resettingTimer.restart();
            }
        }
    }

    function resetBanana() {
        state = "invisible"
        xpos = ndx*40+15;
        ypos = 20 + Math.random()*100;
        initialRotation = Math.random() * 90;
        rotation = initialRotation;
        resettingTimer.interval = 5000 + Math.random() * 5000;
        lifeTimer.interval = 5000 + Math.random() * 5000;
    }

    function createBanana() {
        resetBanana();
        if (Math.random() < 0.5) {
            // starts alive?
            state = "normal"
            lifeTimer.restart();
        } else {
            state = "invisible"
            resettingTimer.restart();
        }
    }

    function respawnBanana() {
        resetBanana();
        state = "normal"
        bananaAppearAnimation.start();
    }

    function monkeyTouched() {
        if (!monkey.falling &&
                score < scoreLimit &&
                (state == "normal" || state == "falling") &&
                monkey.y < y + height &&
                monkey.y+monkey.height > y &&
                monkey.x < x + width &&
                monkey.x+monkey.width > x ) {
            // touched banana
            catchBanana();
        }
    }

    function catchBanana() {
        state = "catched"
        lifeTimer.stop();
        resettingTimer.restart();
        bananaCatchAnimation.start();
    }

    function bananaFall() {
        bananaFallAnimation.start();
    }
}
