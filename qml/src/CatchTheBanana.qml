import QtQuick 2.0
import "catchthebanana.js" as Functions

Rectangle {
    id: rootWindow

    width: 320
    height: 460

    property int scoreLimit: 17
    property real frontScale: 1.5
    property int bananaCount: 17
    property int coconutCount: 10

    property int score : 0;
    onScoreChanged: if (score == scoreLimit) {
        victoryScreen.visible = true
        monkey.resetMonkey();
        gamescreen.visible = false
    }

    signal resetItems

    Component.onCompleted: start();
    function start() {
        gamescreen.visible = true
        score = 0
        monkey.resetMonkey();
        resetItems();
        victoryScreen.visible = false
    }

    Item {
        id: gamescreen
        anchors.fill: parent
        visible: true

        // backgrounds
//        Image {
//            id: background
//            anchors.fill: parent
//            source: "../pics/bgr.png"
//        }
        Image {
            source:"../../images/bgr4.png"
            anchors.fill: parent
        }
        Image {
            source:"../../images/bgr3.png"
            x: -monkey.x / 6 * frontScale
            y: -36
        }
        Image {
            source:"../../images/bgr2.png"
            x: -monkey.x / 3 * frontScale
            y: -36
        }
        Image {
            source:"../../images/bgr1.png"
            x: -monkey.x * frontScale
            y: -36
        }

        Monkey {
            id: monkey
        }

        // bananas
        Repeater {
            model: bananaCount
            delegate: Banana {
                ndx: index
            }
        }

        // coconuts
        Repeater {
            model: coconutCount
            delegate: Coconut {
                ndx: index
            }
        }

        Basket {
            id: basket
            x : xpos - monkey.x * frontScale
            property int xpos: rootWindow.width - width
        }

        Bird {
            id: bird
        }
    }


    Image {
        id: victoryScreen
        visible: false
        anchors.fill: parent
        source: "../../images/win.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                start();
            }
        }
    }
}

