import QtQuick 2.0

Image {
    // 22x28
    // bird is 168 x 95
    id: coconut
    source: "../../images/coconut.png"

    property int ndx: 0

    property int monkeyY: monkey.y
    onMonkeyYChanged: monkeyTouched();
    onYChanged: {
        birdTouched();
        monkeyTouched();
    }
    property int initialRotation : 0

    property int xpos: 0
    property int ypos: 0
    x: xpos - monkey.x * frontScale
    y: ypos

    Connections {
        target: rootWindow
        onResetItems: createCoconut();
    }

    state : "invisible"
    states: [
        State {
            name: "invisible"
            PropertyChanges {
                target: coconut
                visible: false
            }
        },
        State {
            name: "appearing"
        },
        State {
            name: "normal"
        },
        State {
            name: "falling"
        },
        State {
            name: "catched"
        }
    ]

    Timer {
        id: resettingTimer;
        running: false
        interval: 5000
        repeat: false
        onTriggered: respawnCoconut();
    }

    Timer {
        id: lifeTimer;
        running: false
        interval: 5000
        repeat: false
        onTriggered: coconutFall();
    }

    SequentialAnimation {
        id: coconutCatchAnimation
        property int animDuration: 2000
        ParallelAnimation {
            RotationAnimation {
                target: coconut
                property: "rotation"
                to: 600
                duration: coconutCatchAnimation.animDuration
            }
            PropertyAnimation {
                target: coconut
                property: "xpos"
                to: Math.random() * 200 - 100 + xpos
                duration: coconutCatchAnimation.animDuration
            }
            SequentialAnimation {
                PropertyAnimation {
                    target: coconut
                    property: "ypos"
                    to: ypos - 50
                    duration: coconutCatchAnimation.animDuration / 4
                    easing.type: Easing.OutQuad
                }
                PropertyAnimation {
                    target: coconut
                    property: "ypos"
                    to: rootWindow.height + ypos
                    duration: coconutCatchAnimation.animDuration / 2
                    easing.type: Easing.InQuad
                }
            }
        }
        ScriptAction {
            script: {
                coconut.state = "invisible"
                resettingTimer.restart();
            }
        }
    }

    SequentialAnimation {
        id: coconutAppearAnimation
        PropertyAnimation {
            target: coconut
            property: "scale"
            easing.type: Easing.OutElastic
            from: 0
            to: 1
            duration: 2000
        }
        ScriptAction {
            script: {
                coconut.state = "normal"
                lifeTimer.restart();
            }
        }
    }


    SequentialAnimation {
        id: coconutFallAnimation
        property int animationDuration: 1500

        SequentialAnimation {
            loops: 8
            RotationAnimation {
                target: coconut
                property: "rotation"
                direction: RotationAnimation.Counterclockwise
                to: initialRotation - 15
                duration: 100
            }
            RotationAnimation {
                target: coconut
                property: "rotation"
                direction: RotationAnimation.Clockwise
                to: initialRotation
                duration: 100
            }
        }
//        ScriptAction {
//            script: {
//                coconut.state = "falling"
//            }
//        }
        ParallelAnimation {
            RotationAnimation {
                target: coconut
                property: "rotation"
                direction: RotationAnimation.Counterclockwise
                to: rotation - 360
                duration: coconutFallAnimation.animationDuration
            }
            PropertyAnimation {
                target: coconut
                property: "ypos"
                easing.type: Easing.InQuad
                to: rootWindow.height + ypos
                duration: coconutFallAnimation.animationDuration
            }
        }
        ScriptAction {
            script: {
                coconut.state = "invisible"
                resettingTimer.restart();
            }
        }
    }

    function monkeyTouched() {
        if ((state == "normal" || state == "falling") &&
                !monkey.falling &&
                monkey.y < y + height &&
                monkey.y + monkey.height > y &&
                monkey.x < x + width &&
                monkey.x + monkey.width > x ) {
            // touched coconut
//            score = score - 4;
//            if (score < 0)
//                score = 0;

            state = "catched"
            monkey.fall();
            coconutFallAnimation.stop();
            coconutCatchAnimation.start();
        }
    }

    function birdTouched() {
        if (state == "falling" && bird.state != "falling" &&
                bird.y < y + height &&
                bird.y + bird.height > y &&
                bird.x < x + width &&
                bird.x + bird.width > x ) {

            state = "catched"
            bird.fall();
            coconutFallAnimation.stop();
            coconutCatchAnimation.start();
        }
    }

    function resetCoconut() {
        state = "invisible"
        xpos = 10 + Math.random() * 700;
        ypos = 20 + Math.random()*100;
        initialRotation = 0;
        rotation = initialRotation;
        resettingTimer.interval = 5000 + Math.random() * 5000;
        lifeTimer.interval = 5000 + Math.random() * 5000;
    }

    function createCoconut() {
        resetCoconut();
        if (Math.random() < 0.5) {
            // starts alive?
            state = "normal"
            lifeTimer.restart();
        } else {
            state = "invisible"
            resettingTimer.restart();
        }
    }

    function respawnCoconut() {
        resetCoconut();
        state = "appearing"
        coconutAppearAnimation.start();
    }

    function coconutFall() {
        state = "falling"
        coconutFallAnimation.start();
    }
}
