import QtQuick 2.0


Image {
    id: monkey
    source: "../../images/walkingmonk.png"

    property bool falling: false

    // position
    x: 10
    property int bottomY: rootWindow.height - height - 30
    property int topY: 20
    y: 0
    Behavior on x {
        NumberAnimation { duration: 150 }
    }

    function resetMonkey() {
        x = 30
        y = bottomY
    }

    // move
    MouseArea {
        id: monkeyArea
        anchors.fill: parent
        anchors.margins: 10
        drag.axis: Drag.XAxis
        drag.target: parent
        drag.minimumX: 0
        drag.maximumX: rootWindow.width - parent.width

        onReleased: {
            if (enabled)
                monkeyJumpAnimation.start();
        }
    }

    // jump
    SequentialAnimation {
        id: monkeyJumpAnimation
        ScriptAction { script: { monkeyArea.enabled = false; monkey.source="../../images/jumpmonk.png"; } }
        NumberAnimation { target: monkey; property: "y"; to: monkey.topY; duration: 500; easing.type: Easing.OutQuad }
        PauseAnimation { duration: 100 }
        NumberAnimation { target: monkey; property: "y"; to: (monkey.topY+monkey.bottomY)/2; duration: 400; easing.type: Easing.InQuad }
        NumberAnimation { target: monkey; property: "y"; to: monkey.bottomY; duration: 400; easing.type: Easing.OutBounce }
        ScriptAction { script: { monkeyArea.enabled = true; monkey.source = "../../images/walkingmonk.png";} }
    }

    function fall() {
        monkeyJumpAnimation.stop();
        monkeyFallAnimation.start();
    }
    // fall
    SequentialAnimation {
        id: monkeyFallAnimation
        ScriptAction { script: { monkeyArea.enabled = false; monkeyArea.drag.target=null; monkey.source="../../images/sittingmonk.png"; falling=true; } }
        NumberAnimation { target: monkey; property: "y"; to: monkey.bottomY;
            //duration: Math.max(100, 1000 * (monkey.bottomY - y) / (monkey.bottomY - monkey.topY))
            duration: 1400
            easing.type: Easing.OutBounce }
        PauseAnimation { duration : 2000 }
        ScriptAction { script: { monkeyArea.enabled = true; monkeyArea.drag.target=monkey; monkey.source = "../../images/walkingmonk.png"; falling=false; } }
    }
}
