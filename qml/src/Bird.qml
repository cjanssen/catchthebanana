// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.0

Image {
    id: bird
    source: "../../images/bird.png"
    width: 84
    height: 47

    property int xpos: 0
    property int ypos: 0
    x: xpos - monkey.x * frontScale
    y: ypos

    property int monkeyY: monkey.y
    onMonkeyYChanged: monkeyTouched();

    state: "invisible"

    states: [
        State {
            name: "invisible"
            PropertyChanges {
                target: bird
                visible: false
            }
        },
        State {
            name: "normal"
        },
        State {
            name: "eating"
        },
        State {
            name: "falling"
        }
    ]

    Timer {
        id: spawnTimer
        interval: 1000
        running: true
        repeat: false
        onTriggered: spawn();
    }

    SequentialAnimation {
        id: flyAnimation
        property int spawnX: 0;
        property int descendX: 0;
        PropertyAction {
            target: bird
            property: "ypos"
            value: 20
        }
        ParallelAnimation {
            PropertyAnimation {
                target: bird
                property: "xpos"
                from: flyAnimation.spawnX
                to: flyAnimation.descendX
                duration: 2000
            }
            PropertyAnimation {
                target: bird
                property: "ypos"
                to: 100
                duration: 2000
            }
        }
        ParallelAnimation {
            PropertyAnimation {
                target: bird
                property: "xpos"
                to: basket.xpos
                duration: 2000
            }
            PropertyAnimation {
                target: bird
                property: "ypos"
                to: basket.basketTop
                duration: 2000
            }
        }
        PropertyAction {
            target: bird
            property: "state"
            value: "eating"
        }

        PauseAnimation { duration: 2000 }
//        PropertyAction {
//            target: bird
//            property: "state"
//            value: "invisible"
//        }
        ScriptAction {
            script: {
//                spawnTimer.start();
                score -= 4;
                if (score < 0)
                    score = 0;
                flyAwayAnimation.start();
            }
        }
    }

    SequentialAnimation {
        id: flyAwayAnimation
        property int spawnX: 0;
        property int descendX: 0;
        ScriptAction {
            script: {
                bird.state = "normal"
                if (Math.random() < 0.5) {
                    flyAwayAnimation.spawnX = -width;
                    flyAwayAnimation.descendX = basket.xpos / 2;

                } else {
                    flyAwayAnimation.spawnX = rootWindow.width * frontScale* 3/2;
                    flyAwayAnimation.descendX = (basket.xpos + flyAwayAnimation.spawnX)/2;
                }
            }
        }

        ParallelAnimation {
            PropertyAnimation {
                target: bird
                property: "xpos"
                to: flyAwayAnimation.descendX
                duration: 2000
            }
            PropertyAnimation {
                target: bird
                property: "ypos"
                to: 100
                duration: 2000
            }
        }

        ParallelAnimation {
            PropertyAnimation {
                target: bird
                property: "xpos"
                to: flyAwayAnimation.spawnX;
                duration: 2000
            }
            PropertyAnimation {
                target: bird
                property: "ypos"
                to: 10
                duration: 2000
            }
        }

        PropertyAction {
            target: bird
            property: "state"
            value: "invisible"
        }

        ScriptAction {
            script: {
                spawnTimer.start();
            }
        }
    }


    SequentialAnimation {
        id: fallAnimation

        PropertyAction {
            target: bird
            property: "rotation"
            value: 90
        }

        PropertyAnimation {
            target: bird
            property: "ypos"
            to: rootWindow.height + 10
            duration: 1000
            easing.type: Easing.InQuad
        }

        PropertyAction {
            target: bird
            property: "state"
            value: "invisible"
        }

        PropertyAction {
            target: bird
            property: "rotation"
            value: 0
        }

        ScriptAction {
            script: {
                spawnTimer.start();
            }
        }
    }

    function spawn() {
        if (score == 0) {
            spawnTimer.start();
            return;
        }
        bird.state = "normal"
        if (Math.random() < 0.5) {
            flyAnimation.spawnX = -width;
            flyAnimation.descendX = basket.xpos / 2;

        } else {
            flyAnimation.spawnX = rootWindow.width * frontScale* 3/2;
            flyAnimation.descendX = (basket.xpos + flyAnimation.spawnX)/2;
        }
        flyAnimation.start();
    }

    function fall() {
        bird.state = "falling"
        flyAnimation.stop();
        flyAwayAnimation.stop();
        fallAnimation.start();
    }

    function monkeyTouched() {
        if (state == "normal" &&
                !monkey.falling &&
                monkey.y < y + height &&
                monkey.y + monkey.height > y &&
                monkey.x < x + width &&
                monkey.x + monkey.width > x ) {
            monkey.fall();
            bird.fall();
        }
    }
}
