//
//  CMConstants.h
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef CrazyMonkey_CMConstants_h
#define CrazyMonkey_CMConstants_h

#define GROUND_Y_POSITION                   10.0f
#define CEILING_Y_POSITION                  120.0f

#define SCALE_FACTOR                        1.5
#define BANANA_LIFE_TIME                    5
#define COCONUT_LIFE_TIME                   5
#define NUMBER_OF_BANANAS                   10
#define NUMBER_OF_COCONUTS                  5
#define MAX_BANANA_SCORE                    17

#define MONKEY_X_POSITION_CHANGED           @"MONKEY_X_POSITION_CHANGED"
#define MONKEY_Y_POSITION_CHANGED           @"MONKEY_Y_POSITION_CHANGED"
#define ADD_BANANA_TO_BASKET                @"ADD_BANANA_TO_BASKET"
#define MONKEY_HIT_COCONUT                  @"MONKEY_HIT_COCONUT"

#endif
