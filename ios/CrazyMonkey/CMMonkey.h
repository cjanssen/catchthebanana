//
//  CMMonkey.h
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Sparrow.h"

@interface CMMonkey : SPDisplayObjectContainer
{
    SPTexture *m_walkingMonkey;
    SPTexture *m_sittingMonkey;
    SPTexture *m_jumpingMonkey;
    SPImage *m_monkey;
    
    SPJuggler *m_juggler;
    
    float m_offsetX;
    float m_offsetY;
    float m_screenWidth;
    float m_screenHeight;
    
    bool m_isGrabbed;
    bool m_isTouchEnabled;
    bool m_isHit;
}

- (id)initWithScreenWidth:(float)width height:(float)height;
- (void)advanceTime:(double)time;
- (float)monkeyX;
- (float)monkeyY;
- (float)monkeyWidth;
- (float)monkeyHeight;
- (bool)dazed;
@end
