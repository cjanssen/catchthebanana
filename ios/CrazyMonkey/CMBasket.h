//
//  CMBasket.h
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Sparrow.h"

@interface CMBasket : SPDisplayObjectContainer
{
    SPImage *m_basket;
    SPTextField *m_score;
    
    NSMutableArray *m_bananas;
    
    float m_screenHeight;
    
    // not owned
    SPTexture *m_bananaTexture;
}

- (id)initWithScreenWidth:(float)width height:(float)height;
- (float)top;
- (void)setBanana:(SPTexture*)banana;
- (NSUInteger)score;

@end
