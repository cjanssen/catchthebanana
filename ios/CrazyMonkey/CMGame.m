//
//  CMGame.m
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CMGame.h"
#import "CMMonkey.h"
#import "CMBackground.h"
#import "CMBasket.h"
#import "CMFruitLayer.h"
#import "CMConstants.h"

@interface CMGame ()
- (void)onEnterFrame:(SPEnterFrameEvent *)event;
@end

@implementation CMGame

- (id)initWithWidth:(float)width height:(float)height
{
    if (self = [super initWithWidth:width height:height])
    {
        m_background = [[CMBackground alloc] init];
        
        m_monkey = [[CMMonkey alloc] initWithScreenWidth:width height:height];
        
        m_basket = [[CMBasket alloc] initWithScreenWidth:width height:height];
        
        SPTexture *bananaTexture = [SPTexture textureWithContentsOfFile:@"banana.png"];
        [m_basket setBanana:bananaTexture];
        
        SPTexture *coconutTexture = [SPTexture textureWithContentsOfFile:@"coconut.png"];
        
        m_fruitLayer = [[CMFruitLayer alloc] initWithScreenWidth:width height:height banana:bananaTexture coconut:coconutTexture];
        [m_fruitLayer setMonkey:m_monkey];
        [m_fruitLayer setBasketTop:[m_basket top]];
        [self addChild:m_background];
        [self addChild:m_basket];
        [self addChild:m_monkey];
        [self addChild:m_fruitLayer];
        
//        m_bananas = [[NSMutableArray alloc] initWithCapacity:NUMBER_OF_BANANAS];
//        for (int i = 0; i < NUMBER_OF_BANANAS; ++i) {
//            CMBanana *banana = [[[CMBanana alloc] initWithScreenWidth:width height:height texture:bananaTexture index:i] autorelease];
//            [banana setBasketTop:[m_basket top]];
//            [banana setMonkey:m_monkey];
//            [self addChild:banana];
//            [m_bananas addObject:banana];
//        }
//        
//        m_coconuts = [[NSMutableArray alloc] initWithCapacity:NUMBER_OF_COCONUTS];
//        SPTexture *coconutTexture = [SPTexture textureWithContentsOfFile:@"coconut.png"];
//        for (int i = 0; i < NUMBER_OF_COCONUTS; ++i) {
//            CMCoconutLayer *coconut = [[[CMCoconutLayer alloc] initWithScreenWidth:width height:height texture:coconutTexture index:i] autorelease];
//            [coconut setMonkey:m_monkey];
//            [self addChild:coconut];
//            [m_coconuts addObject:coconut];
//        }
//        
        [self addEventListener:@selector(onEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
    }
    return self;
}

#pragma mark -
#pragma mark Memory Managment

- (void)dealloc
{
    [m_monkey release];
    [m_background release];
    [m_bananas release];
    [m_coconuts release];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Notifications

- (void)onEnterFrame:(SPEnterFrameEvent *)event
{
    double passedTime = event.passedTime;
    
    [m_monkey advanceTime:passedTime];
    [m_fruitLayer advanceTime:passedTime];
    for (int i = 0; i < NUMBER_OF_BANANAS; ++i)
        [[m_bananas objectAtIndex:i] advanceTime:passedTime];
    for (int i = 0; i < NUMBER_OF_COCONUTS; ++i)
        [[m_coconuts objectAtIndex:i] advanceTime:passedTime];
}

@end
