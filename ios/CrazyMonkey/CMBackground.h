//
//  CMBackground.h
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Sparrow.h"

@interface CMBackground : SPDisplayObjectContainer
{
    SPImage *m_foreground3;
    SPImage *m_foreground2;
    SPImage *m_foreground1;
}
@end
