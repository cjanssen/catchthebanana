//
//  CMBackground.m
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CMBackground.h"
#import "CMMonkey.h"
#import "CMConstants.h"

@interface CMBackground ()
- (void)onMonkeyXPositionChanged:(NSNotification *)aNotification;
@end

@implementation CMBackground

- (id)init
{
    self = [super init];
    if (self) {
        SPImage *background = [SPImage imageWithContentsOfFile:@"bgr4.png"];
        m_foreground3 = [[SPImage alloc] initWithTexture:[SPTexture textureWithContentsOfFile:@"bgr3.png"]];
        m_foreground2 = [[SPImage alloc] initWithTexture:[SPTexture textureWithContentsOfFile:@"bgr2.png"]];
        m_foreground1 = [[SPImage alloc] initWithTexture:[SPTexture textureWithContentsOfFile:@"bgr1.png"]];
    
        [m_foreground1 setY:36];
        [m_foreground2 setY:36];
        [m_foreground3 setY:36];
        
        [self addChild:background];
        [self addChild:m_foreground3];
        [self addChild:m_foreground2];
        [self addChild:m_foreground1];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMonkeyXPositionChanged:) name:MONKEY_X_POSITION_CHANGED object:nil];
    }
    return self;
}

#pragma mark -
#pragma mark Memory Managment
- (void)dealloc
{
    [m_foreground1 release];
    [m_foreground2 release];
    [m_foreground3 release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MONKEY_X_POSITION_CHANGED object:nil];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Notifications

- (void)onMonkeyXPositionChanged:(NSNotification *)aNotification
{
    float scaledX = -[(CMMonkey*)aNotification.object monkeyX] * SCALE_FACTOR;
    [m_foreground3 setX:scaledX/6];
    [m_foreground2 setX:scaledX/3];
    [m_foreground1 setX:scaledX];
}
@end