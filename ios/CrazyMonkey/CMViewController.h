//
//  CMViewController.h
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sparrow.h"

@interface CMViewController : UIViewController
{
    SPView *m_sparrowView;
}

- (id)initWithFrame:(CGRect)frame;

@property (nonatomic, retain) SPStage *stage;

@end
