//
//  CMGame.h
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sparrow.h"

@class CMMonkey;
@class CMBackground;
@class CMBasket;
@class CMFruitLayer;

@interface CMGame : SPStage
{
    CMBackground *m_background;
    CMMonkey *m_monkey;    
    CMBasket *m_basket;
    CMFruitLayer *m_fruitLayer;
    
    NSMutableArray *m_bananas;
    NSMutableArray *m_coconuts;
}
@end
