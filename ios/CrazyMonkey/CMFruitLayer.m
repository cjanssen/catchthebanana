//
//  CMCoconut.m
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CMFruitLayer.h"
#import "CMConstants.h"
#import "CMMonkey.h"

#define BANANA_TYPE 0
#define COCOCNUT_TYPE 1

@interface CMFruit: SPImage
{
    double m_lifetime;
    NSUInteger m_fruitType;
    bool m_caught;
    bool m_ripe;
    int m_oscillateCount;
}

- (void)setLifetime:(double)time;
- (double)lifetime;
- (void)setFruitType:(NSUInteger)fruitType;
- (NSUInteger)fruitType;
- (void)setCaught:(bool)caught;
- (bool)caught;
- (void)setRipe:(bool)ripe;
- (bool)ripe;
- (void)setOscillateCount:(int)oscillateCount;
- (int)oscillateCount;
@end

@interface CMFruitLayer ()
- (void)onMonkeyXPositionChanged:(NSNotification *)aNotification;
- (void)onMonkeyYPositionChanged:(NSNotification *)aNotification;
- (void)onFallDown:(SPEvent*)event;
- (void)onFallingDown:(SPEvent*)event;
- (void)onOscillate:(SPEvent*)event;
- (void)onAddedToBasket:(SPEvent*)event;
- (void)spawnFruit:(CMFruit*)fruit;
- (void)addToSpawnCycle:(CMFruit*)fruit;
- (void)oscillateFruit:(CMFruit*)fruit;
- (bool)checkCollision:(CMFruit*)fruit;
- (void)animateFallDown:(CMFruit*)fruit;
@end

@implementation CMFruitLayer

- (id)initWithScreenWidth:(float)width height:(float)height banana:(SPTexture*)bananaTexture coconut:(SPTexture*)coconutTexture
{
    self = [super init];
    if (self) {
        
        m_screenHeight = height;
        m_screenWidth = width;
        m_juggler = [[SPJuggler alloc] init];
        m_fruits = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < NUMBER_OF_COCONUTS; ++i) {
            CMFruit *coconut = [[CMFruit alloc] initWithTexture:coconutTexture];
            [coconut setPivotX:coconut.width/2];
            [coconut setPivotY:coconut.height/2];
            [coconut setFruitType:COCOCNUT_TYPE];
            [coconut setVisible:YES];
            [m_fruits addObject:coconut];
            [self addChild:coconut];
            [self addToSpawnCycle:coconut];
        }
        
        for (int i = 0; i < NUMBER_OF_BANANAS; ++i) {
            CMFruit *banana = [[CMFruit alloc] initWithTexture:bananaTexture];
            [banana setPivotX:banana.width/2];
            [banana setPivotY:banana.height/2];
            [banana setFruitType:BANANA_TYPE];
            [banana setVisible:YES];
            [m_fruits addObject:banana];
            [self addChild:banana];
            [self addToSpawnCycle:banana];
        }

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMonkeyYPositionChanged:) name:MONKEY_Y_POSITION_CHANGED object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMonkeyXPositionChanged:) name:MONKEY_X_POSITION_CHANGED object:nil];
    }
    return self;
}

#pragma mark -
#pragma mark Memory Managment

- (void)dealloc
{
    [m_juggler release];
    [m_fruits release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MONKEY_Y_POSITION_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MONKEY_X_POSITION_CHANGED object:nil];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Notifications

- (void)onMonkeyXPositionChanged:(NSNotification *)aNotification
{
//    float scaledX = -[(CMMonkey*)aNotification.object monkeyX] * SCALE_FACTOR;
//    for (int j = 0; j < m_fruits.count; ++j) {
//        CMFruit *fruit = (CMFruit*)[m_fruits objectAtIndex:j];
//        [fruit setX:fruit.x - scaledX];           
//    }
}

- (void)onMonkeyYPositionChanged:(NSNotification *)aNotification
{
    // check if it collides with a fruit
    NSMutableArray *fruitsHit = [[NSMutableArray alloc] init]; 
    for (int j = 0; j < m_fruits.count; ++j) {
        CMFruit *fruit = (CMFruit*)[m_fruits objectAtIndex:j];
        if ([self checkCollision:fruit])
            [fruitsHit addObject:fruit];            
    }
        
    if (fruitsHit.count == 0) {
        [fruitsHit release];
        return;
    }
    
    for (int k = 0; k < fruitsHit.count; ++k) {
        CMFruit *fruit = (CMFruit*)[fruitsHit objectAtIndex:k];
        [self animateFallDown:fruit];
    }
    [fruitsHit release];
}

- (void)onFallDown:(SPEvent*)event
{
    CMFruit *fruit = (CMFruit*)[(SPTween *)event.target target];
    [self addToSpawnCycle:fruit];
}

- (void)onFallingDown:(SPEvent*)event
{
    CMFruit *fruit = (CMFruit*)[(SPTween *)event.target target];
    if ([self checkCollision:fruit])
        [self animateFallDown:fruit];
}

- (void)onOscillate:(SPEvent*)event
{
    CMFruit *fruit = (CMFruit*)[(SPTween *)event.target target];
    [self oscillateFruit:fruit];
}

- (void)onAddedToBasket:(SPEvent*)event
{
    [[NSNotificationCenter defaultCenter] postNotificationName:ADD_BANANA_TO_BASKET object:self];
    CMFruit *fruit = (CMFruit*)[(SPTween *)event.target target];
    [self addToSpawnCycle:fruit];
}

#pragma mark -
#pragma mark Accessors

- (void)advanceTime:(double)seconds
{
//   NSLog(@"Time passed since last frame: %f %d", seconds, m_fruits.count);
    [m_juggler advanceTime:seconds];
    
    NSMutableArray *ripeFruits = [[NSMutableArray alloc] init];
    for (int i = 0; i < m_fruits.count; ++i) {
        CMFruit *fruit = (CMFruit*)[m_fruits objectAtIndex:i];
//        NSLog(@"%d %d %d", fruit.visible, [fruit caught],  [fruit ripe]);
        if (!fruit.visible || [fruit caught] || [fruit ripe])
            continue;
        double lifetime = [fruit lifetime] - seconds;
        [fruit setLifetime:lifetime];
        if (lifetime < 0) {
            [ripeFruits addObject:fruit];
        }        
    }
    if (ripeFruits.count == 0) {
        [ripeFruits release];
        return;
    }

    for (int k = 0; k < ripeFruits.count; ++k) {
        CMFruit *fruit = (CMFruit*)[ripeFruits objectAtIndex:k];
        [m_juggler removeObjectsWithTarget:fruit];
        
        [fruit setRipe:YES];
        [self oscillateFruit:fruit];
    }
    [ripeFruits release];        
}

- (void)setMonkey:(CMMonkey*)monkey
{
    m_monkey = monkey;
}

- (void)setBasketTop:(float)top
{
    m_basketTop = top;
}

#pragma mark -
#pragma mark Private

- (void)spawnFruit:(CMFruit*)fruit
{
    // set position
    // set lifetime
    // spawn animation
    
    [fruit setX:(arc4random() % (NSUInteger)(m_screenWidth*SCALE_FACTOR)) /*- [m_monkey monkeyX] * SCALE_FACTOR*/];
    [fruit setY:(arc4random() % 40) + CEILING_Y_POSITION + fruit.height];
    [fruit setScaleX:0.1f];
    [fruit setScaleY:0.1f];
    [fruit setVisible:YES];
    [fruit setCaught:NO];
    [fruit setRipe:NO];
    [fruit setOscillateCount:0];
    [fruit setRotation:0];
    
    if ([fruit fruitType] == BANANA_TYPE)
        [fruit setLifetime:BANANA_LIFE_TIME + arc4random() % 5];
    else
        [fruit setLifetime:COCONUT_LIFE_TIME + arc4random() % 3];

    SPTween *tween = [SPTween tweenWithTarget:fruit time:2 transition:SP_TRANSITION_EASE_OUT_ELASTIC];
    [tween animateProperty:@"scaleX" targetValue:1.0f];
    [tween animateProperty:@"scaleY" targetValue:1.0f];
    [m_juggler addObject:tween]; 
}

- (void)addToSpawnCycle:(CMFruit*)fruit
{
    [fruit setVisible:NO];
    [[m_juggler delayInvocationAtTarget:self byTime:(arc4random()%10)] spawnFruit:fruit];
}

- (void)oscillateFruit:(CMFruit*)fruit
{
    int oscillateCount = [fruit oscillateCount];
    
    SPTween *tween;
    if (oscillateCount > 8) {
        tween = [SPTween tweenWithTarget:fruit time:1.5f];
        [tween animateProperty:@"rotation" targetValue:SP_D2R(360)];
        [tween animateProperty:@"y" targetValue:m_screenHeight];
        [tween addEventListener:@selector(onFallingDown:) atObject:self forType:SP_EVENT_TYPE_TWEEN_UPDATED];
        [tween addEventListener:@selector(onFallDown:) atObject:self forType:SP_EVENT_TYPE_TWEEN_COMPLETED];
    } else {
        float rotateDegree = 0.0f;
        if (oscillateCount%2)
            rotateDegree = SP_D2R(-15);
        else
            rotateDegree = SP_D2R(30);
        [fruit setOscillateCount:++oscillateCount];
        tween = [SPTween tweenWithTarget:fruit time:0.1f];
        [tween animateProperty:@"rotation" targetValue:rotateDegree];
        [tween addEventListener:@selector(onOscillate:) atObject:self forType:SP_EVENT_TYPE_TWEEN_COMPLETED];
        
    }
    [m_juggler addObject:tween];
}

- (bool)checkCollision:(CMFruit*)fruit
{
    // check if it collides with a fruit
    float x1 = [m_monkey monkeyX];
    float x2 = x1 + [m_monkey monkeyWidth];
    float y1 = [m_monkey monkeyY];
    float y2 = y1 + [m_monkey monkeyHeight];
    
    float xx1 = fruit.x;
    float xx2 = xx1 + fruit.width;
    float yy1 = fruit.y;
    float yy2 = yy1 + fruit.height;
    
    if (fruit.visible &&
        ((x1 <= xx1 && xx1 <= x2) || (x1 <= xx2 && xx2 <= x2)) &&
        ((y1 <= yy1 && yy1 <= y2) || (y1 <= yy2 && yy2 <= y2)))
        return YES;
    return NO;
}

- (void)animateFallDown:(CMFruit*)fruit
{
    SPTween *tween;
    if ([fruit fruitType] == BANANA_TYPE) {
        if ([m_monkey dazed])
            return;
        tween = [SPTween tweenWithTarget:fruit time:1];
        [tween animateProperty:@"rotation" targetValue:SP_D2R(600)];
        [tween animateProperty:@"x" targetValue:0];
        [tween animateProperty:@"y" targetValue:m_basketTop];
        [tween addEventListener:@selector(onAddedToBasket:) atObject:self forType:SP_EVENT_TYPE_TWEEN_COMPLETED];
    } else {
        tween = [SPTween tweenWithTarget:fruit time:1];
        [tween animateProperty:@"rotation" targetValue:SP_D2R(300)];
        [tween animateProperty:@"x" targetValue:(arc4random() % (NSUInteger)m_screenWidth)];
        [tween animateProperty:@"y" targetValue:m_screenHeight];
        [tween addEventListener:@selector(onFallDown:) atObject:self forType:SP_EVENT_TYPE_TWEEN_COMPLETED];
        [[NSNotificationCenter defaultCenter] postNotificationName:MONKEY_HIT_COCONUT object:self];
    }
    [m_juggler removeObjectsWithTarget:fruit];
    [fruit setCaught:YES];
    [m_juggler addObject:tween];
}

@end

@implementation CMFruit
- (void)setLifetime:(double)time
{
    m_lifetime = time;
}

- (double)lifetime
{
    return m_lifetime;
}

- (void)setFruitType:(NSUInteger)fruitType
{
    m_fruitType = fruitType;
}

- (NSUInteger)fruitType
{
    return m_fruitType;
}

- (void)setCaught:(bool)caught
{
    m_caught = caught;
}

- (bool)caught
{
    return m_caught;
}

- (void)setRipe:(bool)ripe
{
    m_ripe = ripe;
}

- (bool)ripe
{
    return m_ripe;
}

- (void)setOscillateCount:(int)oscillateCount
{
    m_oscillateCount = oscillateCount;
}

- (int)oscillateCount
{
    return m_oscillateCount;
}

@end