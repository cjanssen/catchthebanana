//
//  CMCoconut.h
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Sparrow.h"

@class CMMonkey;

@interface CMFruitLayer : SPDisplayObjectContainer
{    
    SPJuggler *m_juggler;
    
    NSMutableArray *m_fruits;
    
    float m_screenHeight;
    float m_screenWidth;
    float m_basketTop;
    // Not owned
    CMMonkey *m_monkey;
}
- (id)initWithScreenWidth:(float)width height:(float)height banana:(SPTexture*)bananaTexture coconut:(SPTexture*)coconutTexture;
- (void)advanceTime:(double)time;
- (void)setMonkey:(CMMonkey*)monkey;
- (void)setBasketTop:(float)top;

@end
